import React from 'react';
import './App.css';

class App extends React.Component {
    render(){
        return (
            <Families key="all-families"/>
        );
    }
}

class Families extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            families: [],
            newFamily: ''
        };

        this.loadFamilies = this.loadFamilies.bind(this);
        this.newFamily = this.newFamily.bind(this);
        this.deleteFamily = this.deleteFamily.bind(this);

        this.handleNewFamilyField = this.handleNewFamilyField.bind(this);
        
        this.loadFamilies();
    }

    newFamily(e) {
        e.preventDefault();
        if(this.state.newFamily === ''){
            return;
        }
        const http = new XMLHttpRequest();
        http.open('POST', 'http://localhost:4000/families/');
        http.setRequestHeader('Content-Type', 'application/json');
        http.onload = ()=>{
            this.loadFamilies();
        }
        http.send(JSON.stringify({
            name: this.state.newFamily,
            members: [],
            creation_date: Date.now().toString()
        }));
    }

    loadFamilies(){
        const http = new XMLHttpRequest();
        http.open('GET', 'http://localhost:4000/families?_sort=creation_date&_order=desc', true);
        http.onload = (families)=>{
            console.log('Families.loadFamilies - Request Done: HTTP return');
            console.log(families);
            let newOne = this.state;
            newOne.families = JSON.parse(families.target.response);
            newOne.newFamily = '';

            this.setState(newOne);
        };
        http.send();
    }

    handleNewFamilyField(e){
        let newOne = this.state;
        newOne.newFamily = e.target.value;

        this.setState(newOne);
    }

    deleteFamily(id){
        const http = new XMLHttpRequest();
        http.open('DELETE', 'http://localhost:4000/families/' + id, true);
        http.onload = () => {
            this.loadFamilies();
        };
        http.send();
    }

    render() {
        if(this.state.length < 1){
            console.log("Families.render() - No families yet")
            return (<p>Nothing for a while</p>);
        }else{
            console.log('Families.Render - Families.state');
            console.log(this.state);
            
            return (
                <div className="container">
                    <div className="col-12 mb-5">
                        <h3>New Family</h3>
                        <form className="input-group" onSubmit={this.newFamily}>
                            <input type="text" name="family-name" className="form-control" onChange={this.handleNewFamilyField} placeholder="New Family's surname" />
                            <div className="input-group-append">
                                <input type="submit" value="New Family" className="btn btn-primary"/>
                            </div>
                        </form>
                    </div>
                    <div className="col-12">
                        {
                            this.state.families.map(family=>{
                                return <Family data={family} key={family.id} deleteSelf={this.deleteFamily} loadFamilies={this.loadFamilies} />
                            })
                        }
                    </div>
                </div>
            );
        }
    }
}

class Family extends React.Component {

    constructor(props){
        super(props);
        this.handleNewMember = this.handleNewMember.bind(this);
        
        let newOne = this.props.data;
        newOne.newMember = '';

        this.state = newOne;
    }

    newMember(){
        const http = new XMLHttpRequest();
        http.open('POST', 'http://localhost:4000/families/' + this.state.id + '/members/');
        http.setRequestHeader('Content-Type', 'application/json');
        http.onload = ()=>{
            this.props.loadFamilies();
        }
        http.send(JSON.stringify({
            name: this.state.newMember
        }));
    }

    handleNewMember(e) {
        let newOne = this.state;
        newOne.newMember = e.target.value;

        this.setState(newOne);
    }

    renderMembers(){
        console.log("Family.renderMembers - Family.state.members");
        console.log(this.state.members);
        if(this.state.members !== []){
            return this.state.members.map((data) => 
                <Member name={data} key={data} />
            );
        }
        else{
            return (<p>No members</p>);
        }
    }

    render(){
        return(
            <div className="card mb-3">
                <h5 className="card-header">
                    <span>{this.props.data.name}'s family</span>
                    <a href="#delete" onClick={()=>{this.props.deleteSelf(this.props.data.id)}} className="float-right">X</a>
                </h5>
                <div className="card-body">
                    <ul className="list-group list-group-flush">
                        {this.renderMembers()}
                    </ul>
                </div>
                <div className="card-footer">
                    <div className="input-group">
                        <input type="text" name="member" className="form-control" onChange={this.handleNewMember} placeholder="New member's name"/>
                        <div className="input-group-append">
                            <input type="button" value="New Member" onClick={()=>{this.newMember()}} className="btn btn-primary"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function Member(props){
    return (
        <li className="list-group-item">
            <strong>{props.name}</strong><a href="#delete" className="float-right">X</a>
        </li>
    )
}

export function Navbar(){
    return (
        <nav className="navbar navbar-dark bg-dark mb-5">
            <a href="/" className="navbar-brand">React Families</a>
        </nav>
    );
}

export default App;
